import math


def get_lead_time():
	'''Gets the lead time variable
	
	Returns:
		list -- list containing lead times
	'''
	lead_time = [6.9, 6.5, 4.3, 6.9, 6.0, 6.9, 5.8, 7.3, 4.5, 6.3]
	return lead_time


def get_demand():
	'''Gets the demand variab	
	
	Returns:
		list -- list containing demands
	'''
	demand = [103, 83, 116, 97, 112, 104, 106, 109, 92, 96]
	return demand


def mean(data):
	'''Calculates the mean
	
	Arguments:
		data {list} -- list to get the mean of 
	
	Returns:
		float -- mean of the input list
	'''
	return float(sum(data)) / max(len(data), 1)


def calculate_covariance(lead_time, demand):
	'''Calculates the covariance
	
	Arguments:
		lead_time {list} -- list of lead times
		demand {list} -- list of demands
	
	Returns:
		float -- covariance of the two input list
	'''
	n = len(lead_time)
	mean_lead_time = mean(lead_time)
	mean_demand = mean(demand)
	summation = 0
	for i in range(0,10):
		summation += lead_time[i]*demand[i]
	cov = (summation - n*mean_lead_time*mean_demand)/(n-1)
	return cov


def sum_sqaure_list(data):
	'''Calculates the sum of squares of each term in the list 
	
	Arguments:
		data {list} -- list to get the sum of square of terms 
	
	Returns:
		float -- sum of squares of the terms in the list
	'''
	return sum([i ** 2 for i in data])


def calculate_variance(data):
	'''Calculates the variance of the list
	
	Arguments:
		data {list} -- list to get the variance of
	
	Returns:
		float -- variance of the list
	'''
	n = len(data)
	sigma_square = (sum_sqaure_list(data) - n*(mean(data) ** 2))/(n-1)
	return math.sqrt(sigma_square)


def calculate_corelation(cov, sigma1, sigma2):
	'''Calculates the correlation between the two inputs
	
	Arguments:
		cov {float} -- covariance between the two inputs 
		sigma1 {float} -- variance of input1
		sigma2 {float} -- variance of input2
	
	Returns:
		float -- corelation between the two inputs
	'''
	return cov / (sigma1*sigma2)


def main():
	lead_time = get_lead_time()
	demand = get_demand()
	cov = calculate_covariance(lead_time, demand)
	variance_lead_time = calculate_variance(lead_time)
	variance_demand = calculate_variance(demand)
	correlation = calculate_corelation(cov, variance_lead_time, variance_demand)
	print(correlation)


if __name__ == '__main__':
	main()