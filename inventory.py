import pandas as pd

class Data(object):
	def __init__(self):
		self.daily_demand_pd = get_daily_demand_pd()
		self.lead_time_pd = get_lead_time_pd()
		self.rd_demand = get_rd_demand()
		self.rd_lead = get_rd_lead()

class Counter(object):
	def __init__(self):
		self.row = None
		self.lead = None
		
	
def get_zeroes(number):
  zeroes = []
  for i in range(1,number+1):
    zeroes.append(0)
  return zeroes

def get_value_from_range(rd, table):
  value = 0
  for i in range(0,4):
    if rd in range(int(table['rd_start'][i].item()),int(table['rd_end'][i].item())+1):
      value = table['value'][i]
  return value

def get_daily_demand_pd():
	daily_demand_pd = {'value':[1,2,3,4,5],'rd_start':[1,15,38,56,81],'rd_end':[14,37,55,80,100]}
	daily_demand_pd = pd.DataFrame(data = daily_demand_pd)
	return daily_demand_pd

def get_lead_time_pd():
	lead_time_pd = {'value':[1,2,3,4],'rd_start':[1,3,6,10],'rd_end':[2,5,9,9999]}
	lead_time_pd = pd.DataFrame(data = lead_time_pd)
	return lead_time_pd

def get_rd_demand():
	rd_demand = [94,82,46,29,15,3,42,11,89,98,37,30,68,22,45,77,61,56,39,80,73,55,10,32,1]
	return rd_demand

def get_rd_lead():
	rd_lead = [2,6,4,8,5]
	return rd_lead

def get_simulation_table():
	zeroes = get_zeroes(25)
	simulation = {'cycle':zeroes, 'day':zeroes, 'beg_inventory':zeroes, 'rd_demand':zeroes, 'end_inventory':zeroes, 'shortage':zeroes,'order_quantity':zeroes,'rd_lead':zeroes,'days_arrival':zeroes}
	simulation = pd.DataFrame(data = simulation)
	return simulation

def fill_first_row(simulation):
	simulation['cycle'][0] = 1
	simulation['days'][0] = 1
	simulation['beg_inventory'][0] = 4
	simulation['rd_demand'][0] = data.rd_demand[0]

def simulate(simulation):
	simulation['demand'][row] = get_value_from_range(simulation['rd_demand'][row], data.daily_demand_pd)
	end = initial - simulation['demand'][row]
	if end<0:
		simulation['shortage'][row] = 


def fill_table(simulation,row_counter,cycle,day,initial,rd_demand,rd_lead,days_arrival,daily_demand_pd,lead_time_pd,lead_counter):
	arrival_marker = False 
	simulation['cycle'][row_counter] = cycle
	simulation['day'][row_counter] = day
	simulation['beg_inventory'][row_counter] = initial
	simulation['rd_demand'][row_counter] = rd_demand
	simulation['demand'][row_counter] = get_value_from_range(rd_demand,daily_demand_pd)
	end = initial - simulation['demand'][row_counter]
	if end<0:
		simulation['shortage'][row_counter] = simulation['demand'][row_counter] - initial
	else :
		simulation['end_inventory'][row_counter] = end
	if days_arrival > 0:
		days_arrival -= 1
		if days_arrival == 0:
			arrival_marker = True
	if day == 5:
		simulation['order_quantity'][row_counter] = 15 - end
		simulation['rd_lead'][row_counter] = rd_lead[lead_counter]
		days_arrival = get_value_from_range(simulation['rd_lead'][row_counter],lead_time_pd)
	simulation['days_arrival'][row_counter] = days_arrival
	return simulation,arrival_marker


def main():
	simulation = get_simulation_table()
	data = Data()
	counter = Counter()
	counter.row = 0
	counter.lead = 0
	simulation, arrival_marker = fill_table(simulation,row_counter,0,1,1,4,rd_demand[row_counter],0,2,daily_demand_pd,lead_time_pd)
	row_counter += 1
	for i in range(1,6):
		for j in range(1,6):
			if i==1:
				if j==1:
					continue
				if arrival_marker:
					initial = 15 - simulation['beg_inventory'][0]
				else:
					initial = simulation['end_inventory'][row_counter]
			else:
				if arrival_marker:
					initial = simulation['end_inventory'][row_counter] + simulation['order_quantity'][(i-1)*j]
				else:
					initial = simulation['end_inventory'][row_counter]
			if j==5:
				rd_lead = rd_lead
			row_counter += 1
			simulation,arrival_marker = fill_table(simulation,row_counter,i,j,initial,rd_demand[row_counter],simulation['days_arrival'][row_counter-1],daily_demand_pd,lead_time_pd)
	print simulation
	
if __name__ == "__main__":
    main()
