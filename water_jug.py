import sys
class Tree(object):
    def __init__(self):
        self.one = None
        self.two = None
        self.three = None
        self.four = None
        self.data = None


def get_branch(node):
	if node.one == None:
		return 1
	elif node.two == None:
		return 2
	elif node.three == None:
		return 3
	else :
		return 4

def apply_condition(node):
	curr = []
	curr.extend(node.data)
	if curr[0]<4:
		curr[0]=4
		print(1)
	elif curr[1]<3:
		curr[1] = 3
		print(2)
	elif curr[0]>0:
		curr[0] = 0
		print(3)
	elif curr[1]>0:
		curr[1] = 0
		print(4)
	elif (curr[0]+curr[1])>=4 and curr[1]>0:
		curr[1] = curr[1] - (4-curr[0])
		curr[0] = 4
		print(5)
	elif (curr[0]+curr[1])>=3 and curr[0]>0:
		curr[0] = curr[0] - (3-curr[1])
		curr[1] = 3
		print(6)
	elif (curr[0]+curr[1])<=4 and curr[1]>0:
		curr[0] = (curr[0]+curr[1])
		curr[1] = 0
		print(7)
	elif (curr[0]+curr[1])<=3 and curr[0]>0:
		curr[1] = (curr[0]+curr[1])
		curr[0] = 0
		print(8)
	elif curr == [0,2]:
		curr = [2,0]
		print(9)
	else:
		curr = [-1,-1]
		print(-1)
	return curr


def create_tree(node):
	vis_node = apply_condition(node)
	# print(vis_node)
	if node.data not in visited and vis_node != [-1,-1]:
		visited.append(node.data)
		branch = get_branch(node)
		if branch == 1:
			node.one = Tree()
			node.one.data = vis_node
			create_tree(node.one)
		elif branch == 2:
			node.two = Tree()
			node.two.data = vis_node
			create_tree(node.two)
		elif branch == 1:
			node.three = Tree()
			node.three.data = vis_node
			create_tree(node.three)
		else :
			node.four = Tree()
			node.four.data = vis_node
			create_tree(node.four)

def print_tree(node):
	if node != None:
		print(node.data)
		print_tree(node.one)
		print_tree(node.two)
		print_tree(node.three)
		print_tree(node.four)

if __name__ == '__main__':
	visited = []
	root = Tree()
	root.data = [0,0]
	node = root
	create_tree(node)
	# print_tree(root)
	# print(visited)